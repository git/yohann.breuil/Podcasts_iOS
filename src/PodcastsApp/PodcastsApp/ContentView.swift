//
//  ContentView.swift
//  PodcastsApp
//
//  Created by BREUIL Yohann on 14/05/2023.
//

import SwiftUI

struct ContentView: View {
    @State private var selection: Tab = .library
    
    enum Tab {
        case listen
        case explore
        case library
        case search
    }
    
    var body: some View {
        TabView(selection: $selection) {
            LibraryView()
                .tabItem({
                    Label("Écouter", systemImage: "play.circle.fill")
                })
                .tag(Tab.listen)
            LibraryView()
                .tabItem({
                    Label("Explorer", systemImage: "square.grid.2x2.fill")
                })
                .tag(Tab.explore)
            LibraryView()
                .tabItem({
                    Label("Bibliothèque", systemImage: "square.stack.fill")
                })
                .tag(Tab.library)
            LibraryView()
                .tabItem({
                    Label("Rechercher", systemImage: "magnifyingglass")
                })
                .tag(Tab.search)
        }
        .onAppear {
            let appearance = UITabBarAppearance()
            appearance.backgroundEffect = UIBlurEffect(style: .systemUltraThinMaterial)
            appearance.backgroundColor = UIColor(Color.black.opacity(0.2))
            
            // Use this appearance when scrolling behind the TabView:
            UITabBar.appearance().standardAppearance = appearance
            // Use this appearance when scrolled all the way up:
            UITabBar.appearance().scrollEdgeAppearance = appearance
        }
        .overlay(VStack {
            Spacer()
            NowPlayingBar(podcast: podcastList[0])
                .frame(height: 162)
        })
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView()
            ContentView().preferredColorScheme(.dark)
        }
    }
}

