//
//  PodcastsAppApp.swift
//  PodcastsApp
//
//  Created by BREUIL Yohann on 14/05/2023.
//

import SwiftUI

@main
struct PodcastsAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
