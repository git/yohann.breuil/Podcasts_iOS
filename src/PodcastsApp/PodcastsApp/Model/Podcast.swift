//
//  Podcast.swift
//  PodcastsApp
//
//  Created by BREUIL Yohann on 16/05/2023.
//

import Foundation

public struct Podcast : Identifiable {
    public var id: UUID = UUID()
    
    public var name: String
    public var creator: String
    public var coverImage: String
    public var nbStars: Float
    public var nbEvaluations: Int
    public var topic: String
    public var lastUpdateDate: Date
    
    public var episodes: [Episode] = []
}
