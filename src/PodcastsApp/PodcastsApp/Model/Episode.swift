//
//  Episode.swift
//  PodcastsApp
//
//  Created by BREUIL Yohann on 17/05/2023.
//

import Foundation

public struct Episode : Identifiable {
    public var id: UUID = UUID()
    
    public var title: String
    public var description: String
    public var datePublication: Date = Date.now
    public var duration: Int
    public var isPlayed: Bool = false
    public var isDownloaded: Bool = false
}
