//
//  Stub.swift
//  PodcastsApp
//
//  Created by BREUIL Yohann on 17/05/2023.
//

import Foundation

let libraryMenuItem: [MenuItem] = [
    MenuItem(iconName: "square.stack",
             text: "Podcasts"),
    MenuItem(iconName: "checklist.unchecked",
             text: "Chaînes"),
    MenuItem(iconName: "bookmark",
             text: "Enregistrés"),
    MenuItem(iconName: "arrow.down.circle",
             text: "Téléchargés"),
    MenuItem(iconName: "clock",
             text: "Derniers épisodes"),
]

let podcastList : [Podcast] = [
    Podcast(
        name: "Underscore_",
        creator: "Micode",
        coverImage: "underscore_podcast",
        nbStars: 4.7,
        nbEvaluations: 963,
        topic: "Technologies",
        lastUpdateDate: Date.now,
        episodes: [
            Episode(title: "L'histoire de la triche sur CS:GO",
                    description: "Le jeu CS:GO est connu pour être gangrené par la triche, y compris dans les sphères professionnels et la scène esport. Pourquoi CS:GO est particulièrement touché ? D’où ça vient techniquement, et peut-on y faire quelque-chose ? On remonte l’histoire, qui prend racine en 2014, à quelques semaines de la DreamHack Winter, le plus gros évènement CS:GO de l’année !",
                    duration: 32),
            Episode(title: "L'assistant vocal d'Underscore",
                    description: "Nos assistants vocaux ne sont pas très efficaces… mis à part peut-être pour lancer un minuteur. Mais est-il possible d’en créer un de toute pièce, correspondant à nos envies et à ce qu’on aime ? Avec les nouveaux services d’IA sortis ces derniers mois, on a fait le test, et on vous fait une petite démo en direct ! “OK Michel, écoute Underscore_”",
                    duration: 30,
                    isPlayed: true),
            Episode(title: "Le témoignage de cet étudiant arnaqué par le directeur de son école",
                    description: "Un ancien étudiant vient témoigner de son expérience à SUPINFO. Derrière des pratiques parfois douteuses se cachaient en réalité une organisation quasi mafieuse et un patron prêt à tout pour détourner de l’argent ! Aujourd’hui, il semble pourtant avoir récidivé…",
                    duration: 47,
                    isPlayed: true,
                    isDownloaded: true),
            Episode(title: "La révolution du stockage est en marche",
                    description: "Une nouvelle technologie de mémoire, la ReRAM, pourrait bien bouleverser le secteur et faire radicalement chuter son prix dans les années à venir. Édouard, qui a étudié le sujet pendant trois ans, revient pour nous sur cette technologie très prometteuse !",
                    duration: 34),
        ]),
    Podcast(
        name: "Popcorn",
        creator: "DomingoTV",
        coverImage: "popcorn_cover",
        nbStars: 4.9,
        nbEvaluations: 709,
        topic: "Actualité du divertissement",
        lastUpdateDate: Date.now,
        episodes: [
            Episode(title: "Le futur des écrans se déroule devant vous",
                    description: "PP Garcia nous présente Mardi Turfu, la rubrique tech de Popcorn ! Et pour cette semaine il nous parle des futurs des écrans, mais pas que !",
                    duration: 21)
        ]),
    Podcast(
        name: "Un bon moment avec Kyan KHOJANDI et NAVO",
        creator: "Kyan Khojandi & navo",
        coverImage: "unbonmoment_cover",
        nbStars: 4.7,
        nbEvaluations: 1100,
        topic: "Comédie: les interviews",
        lastUpdateDate: Date.now),
    Podcast(
        name: "À bientôt de te revoir",
        creator: "Binge Audio",
        coverImage: "abientotdeterevoir_cover",
        nbStars: 4.8,
        nbEvaluations: 2700,
        topic: "Comédie: les interviews",
        lastUpdateDate: Date.now),
    Podcast(
        name: "301 vues",
        creator: "Cyprien",
        coverImage: "301vues_cover",
        nbStars: 4.6,
        nbEvaluations: 1100,
        topic: "Humour",
        lastUpdateDate: Date.now),
]
