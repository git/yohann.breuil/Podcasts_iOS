//
//  MenuItem.swift
//  PodcastsApp
//
//  Created by BREUIL Yohann on 18/05/2023.
//

import Foundation

public struct MenuItem : Identifiable {
    public var id: UUID = UUID()
    
    public var iconName: String
    public var text: String
}
