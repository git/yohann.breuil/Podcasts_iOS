//
//  LibraryMenuItem.swift
//  PodcastsApp
//
//  Created by BREUIL Yohann on 17/05/2023.
//

import SwiftUI

struct LibraryMenuItem: View {
    var imageName: String
    var text: String
    
    var body: some View {
        HStack {
            Image(systemName: imageName)
                .foregroundColor(.accentColor)
            Text(text)
                .bold()
            Spacer()
            Image(systemName: "chevron.forward")
        }
        .padding(.horizontal)
    }
}

struct LibraryMenuItem_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            LibraryMenuItem(imageName: "square.stack", text: "Podcasts")
            LibraryMenuItem(imageName: "checklist.unchecked", text: "Chaînes")
            
            LibraryMenuItem(imageName: "square.stack", text: "Podcasts").preferredColorScheme(.dark)
            LibraryMenuItem(imageName: "checklist.unchecked", text: "Chaînes").preferredColorScheme(.dark)
        }
        
    }
}
