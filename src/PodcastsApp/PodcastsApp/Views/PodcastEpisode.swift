//
//  PodcastEpisodeDetail.swift
//  PodcastsApp
//
//  Created by BREUIL Yohann on 14/05/2023.
//

import SwiftUI

struct PodcastEpisode: View {
    var episode: Episode
    
    var body: some View {
        VStack(alignment: .leading) {
            // Date
            Text(episode.datePublication.formatted(date: .abbreviated, time: .omitted))
                .font(.caption)
                .bold()
                .textCase(.uppercase)
            
            // Title
            if episode.isPlayed {
                Text(episode.title)
                    .font(.headline)
                    .bold()
            } else {
                Text(episode.title)
                    .font(.headline)
            }
             
            // Description
            Text(episode.description)
                .font(.body)
            
            // Actions
            HStack {
                Button(action: {}) {
                    Image(systemName: "play.fill")
                }
                .buttonStyle(PlainButtonStyle())
                .foregroundColor(Color.purple)
                .background(Color("#747476"))
                .clipShape(Circle())
                
                if episode.isPlayed {
                    ProgressView(value: 0.2)
                        .frame(width: 70)
                    Text("Il reste \(episode.duration)")
                        .foregroundColor(Color.accentColor)
                        .bold()
                } else {
                    Text("\(episode.duration) min")
                        .foregroundColor(Color.accentColor)
                        .bold()
                }
                
                Spacer()
                
                if (episode.isDownloaded) {
                    Image(systemName: "arrow.down.circle.fill")
                }
                Image(systemName: "ellipsis")
                    
            }
        }
        .padding()
        .frame(width: UIScreen.main.bounds.width, height: 160)
    }
}

struct PodcastEpisodeDetail_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            PodcastEpisode(episode: podcastList[0].episodes[0])
            PodcastEpisode(episode: podcastList[0].episodes[0]).preferredColorScheme(.dark)
            
            PodcastEpisode(episode: podcastList[0].episodes[1])
            PodcastEpisode(episode: podcastList[0].episodes[1]).preferredColorScheme(.dark)
            
            PodcastEpisode(episode: podcastList[0].episodes[2])
            PodcastEpisode(episode: podcastList[0].episodes[2]).preferredColorScheme(.dark)
        }
        
    }
}
