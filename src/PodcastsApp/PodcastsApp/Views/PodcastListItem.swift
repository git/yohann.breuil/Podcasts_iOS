//
//  PodcastListDetail.swift
//  PodcastsApp
//
//  Created by BREUIL Yohann on 16/05/2023.
//

import SwiftUI

struct PodcastListItem: View {
    var podcast: Podcast
    
    var body: some View {
        VStack(alignment: .leading) {
            Image(podcast.coverImage)
                .resizable()
                .scaledToFill()
                .cornerRadius(6)
            Text(podcast.name)
                .bold()
            Text("Mise à jour : Il y a 2j")
        }
        .frame(width: 150, height: 150)
    }
}

struct PodcastListDetail_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            PodcastListItem(podcast: podcastList[0])
            PodcastListItem(podcast: podcastList[0]).preferredColorScheme(.dark)
        }
    }
}
