//
//  PodcastCover.swift
//  PodcastsApp
//
//  Created by BREUIL Yohann on 18/05/2023.
//

import SwiftUI

struct PodcastCover: View {
    var podcast: Podcast
    
    var body: some View {
        VStack {
            Image(podcast.coverImage)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 200, height: 200)
                .cornerRadius(8)
                .opacity(20)
                .shadow(radius: 20)
            Text(podcast.name)
                .font(.title2)
                .bold()
            Text(podcast.creator)
                .font(.title3)
            
            Button(action: {}) {
                Image(systemName: "play.fill")
                Text("Reprendre")
                    .font(.headline)
            }
            .bold()
            .frame(width: 250, height: 25, alignment: .center)
            .padding()
            .background(
                RoundedRectangle(
                    cornerRadius: 8,
                    style: .continuous
                )
                .fill(.white))
        }
        .frame(width: UIScreen.main.bounds.width, height: 400)
        .background(Color(uiColor: UIImage(imageLiteralResourceName: podcast.coverImage).averageColor ?? .white))
    }
}

struct PodcastCover_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            PodcastCover(podcast: podcastList[0])
            PodcastCover(podcast: podcastList[0]).preferredColorScheme(.dark)
        }
    }
}
