//
//  LibraryMenu.swift
//  PodcastsApp
//
//  Created by BREUIL Yohann on 17/05/2023.
//

import SwiftUI

struct LibraryMenu: View {
    var body: some View {
        LazyVStack(alignment: .leading) {
            Divider()
            ForEach (libraryMenuItem) { libraryItem in
                NavigationLink(destination: PodcastListView()) {
                    LibraryMenuItem(imageName: libraryItem.iconName,
                                    text: libraryItem.text)
                }
                Divider()
            }
        }
    }
}

struct LibraryMenu_Previews: PreviewProvider {
    static var previews: some View {
        LibraryMenu()
    }
}
