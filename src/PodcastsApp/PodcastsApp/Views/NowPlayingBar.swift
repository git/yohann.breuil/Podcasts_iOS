//
//  NowPlayingBar.swift
//  PodcastsApp
//
//  Created by BREUIL Yohann on 14/05/2023.
//

import SwiftUI

struct NowPlayingBar : View {
    var podcast : Podcast
    
    var body: some View {
        HStack {
            Button(action: {}) {
                HStack {
                    Image(podcast.coverImage)
                        .resizable()
                        .cornerRadius(8)
                        .frame(width: 50, height: 50)
                        .shadow(radius: 6, x: 0, y: 3)
                        
                    
                    VStack(alignment: .leading) {
                        Text(podcast.episodes[0].title)
                            .frame(width: 150)
                            .truncationMode(.tail)
                            .font(.body)
                            .bold()

                        Text("29 Avril 2023")
                            .font(.caption)
                    }
                
                    Spacer()
                }
                .padding()
            }
            .buttonStyle(.plain)
            
            Button(action: {}) {
                Image(systemName: "play.fill").font(.title3)
            }
            .buttonStyle(.plain)
            .padding(.horizontal)
            
            Button(action: {}) {
                Image(systemName: "goforward.30").font(.title3)
            }
            .buttonStyle(.plain)
            .padding(.trailing, 30)
        }
        .frame(width: UIScreen.main.bounds.size.width, height: 65)
    }
}

struct NowPlayingBar_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            NowPlayingBar(podcast: podcastList[0])
            NowPlayingBar(podcast: podcastList[0]).preferredColorScheme(.dark)
        }
    }
}
