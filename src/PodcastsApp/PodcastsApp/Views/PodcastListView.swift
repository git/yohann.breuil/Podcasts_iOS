//
//  PodcastsList.swift
//  PodcastsApp
//
//  Created by BREUIL Yohann on 16/05/2023.
//

import SwiftUI

struct PodcastListView: View {
    let columns = [GridItem(.flexible()), GridItem(.flexible())]
    
    var body: some View {
        NavigationStack {
            ScrollView {
                LazyVGrid(columns: columns, spacing: 70) {
                    ForEach(podcastList) { podcast in
                        NavigationLink(destination: PodcastView(podcast: podcast)) {
                            PodcastListItem(podcast: podcast)
                        }
                    }
                }
            }
            .navigationTitle("Podcasts")
            .toolbar {
                Button(action: {}) {
                    Image(systemName: "ellipsis")
                }
                .clipShape(Circle())
            }
        }
        
    }
}

struct PodcastsList_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            PodcastListView()
            PodcastListView().preferredColorScheme(.dark)
        }
    }
}
