//
//  PodcastsListView.swift
//  PodcastsApp
//
//  Created by BREUIL Yohann on 14/05/2023.
//

import SwiftUI

struct LibraryView: View {
    let columns = [GridItem(.flexible()), GridItem(.flexible())]
    
    var body: some View {
        NavigationStack {
            ScrollView {
                LibraryMenu()
                
                VStack(alignment: .leading) {
                    Text("Mis à jour récemment")
                        .padding()
                        .bold()
                    
                    LazyVGrid(columns: columns, spacing: 75) {
                        ForEach (podcastList) { podcast in
                            NavigationLink(destination: PodcastView(podcast: podcast)) {
                                PodcastListItem(podcast: podcast)
                            }
                        }
                    }
                }
                .padding(.top)
            }
            .navigationTitle("Bibliothèques")
        }
    }
}

struct PodcastsListView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            LibraryView()
            LibraryView().preferredColorScheme(.dark)
        }
        
    }
}
