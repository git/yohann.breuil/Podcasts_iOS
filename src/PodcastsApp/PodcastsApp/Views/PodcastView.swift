//
//  PodcastInfoDetail.swift
//  PodcastsApp
//
//  Created by BREUIL Yohann on 17/05/2023.
//

import SwiftUI

struct PodcastView: View {
    let columns = [GridItem(.flexible())]
    
    var podcast: Podcast
    
    var body: some View {
        NavigationStack {
            ScrollView {
                VStack {
                    PodcastCover(podcast: podcast)
                    
                    LazyVStack {
                        ForEach (podcast.episodes) { episode in
                            PodcastEpisode(episode: episode)
                            Divider()
                        }
                    }
                }
            }
            .toolbar {
                ToolbarItemGroup {
                    Button(action: {}) {
                        Image(systemName: "checkmark")
                    }
                    .frame(width: 30, height: 30)
                    .clipShape(Circle())
                    .buttonStyle(.bordered)
                    
                    Button(action: {}) {
                        Image(systemName: "ellipsis")
                    }
                    .frame(width: 30, height: 30)
                    .clipShape(Circle())
                    .buttonStyle(.bordered)
                }
            }
            .listStyle(.grouped)
        }
    }
}

struct PodcastInfoDetail_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            PodcastView(podcast: podcastList[0])
            PodcastView(podcast: podcastList[0]).preferredColorScheme(.dark)
        }
        
    }
}
