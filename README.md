# Podcasts iOS app

## 🎯 Goal

Clone Apple Podcasts app in SwiftUI

## 📱 Views 

| Library | Podcasts list | Podcast view |
| -- | -- | -- |
| <img src="./.github/library.png" width="225" height="500"> | <img src="./.github/podcasts-list.png" width="225" height="500"> | <img src="./.github/podcast.png" width="225" height="500"> | 

## 👨‍💻 Author 

**BREUIL Yohann**

* GitHub: [@DJYohann](https://github.com/DJYohann)
* LinkedIn: [@BREUIL Yohann](https://www.linkedin.com/in/yohann-breuil-02b18a165/)
